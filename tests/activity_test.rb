require 'minitest/autorun'
require_relative '../lib/Activity'
class ActivityTest < Minitest::Test

  # Called before every test method runs. Can be used
  # to set up fixture information.
  def setup
    @activity=Activity.new(1,"Test",10,'01.01.2000')
  end

  # Called after every test method runs. Can be used to tear
  # down fixture information.

  def teardown
    # Do nothing
  end

  def test_good_id
    @activity=Activity.new(1,"Test",10,'24.05.2015')
  end

  def test_bad_id
    assert_raises(ArgumentError){
      @activity=Activity.new('hallowelt','Test',10,'24.05.2015')
      puts @activity
    }
  end

  def test_valid_date
    @activity.date= '02.05.2015'
    assert_equal(@activity.date,Date.parse('02.05.2015'), 'Dates are not equal!')
  end

  def test_invalid_date
    assert_raises(ArgumentError){
      @activity.date='5'
    }
  end
end