require 'minitest/autorun'
require_relative '../lib/ActivityList'
require_relative '../lib/Activity'
class Activity_List_Test < Minitest::Test

  # Called before every test method runs. Can be used
  # to set up fixture information.
  def setup
    @acl=ActivityList.new
    @acl.add_activity(Activity.new(1,"T1",5,'03.05.2015'))
  end

  # Called after every test method runs. Can be used to tear
  # down fixture information.

  def teardown
    # Do nothing
  end

  def test_insert
    @acl.clear
    max_id=0
    sum=0
    100.times do
      id=Random.rand.to_i.abs
      dur=Random.rand.to_i.abs+1
      sum+=dur
      max_id=id+1 if id >= max_id
      @acl.add_activity(Activity.new(id,"T",dur,'02.05.2015'))
      assert_equal(@acl.get_activity(id).duration,dur)
      assert_equal(@acl.sum_minutes,sum)
      assert_equal(@acl.get_next_id,max_id)
    end
  end

  def test_remove_fail
    assert_nil(@acl.remove_activity(3))
  end

  def test_remove_success

  end
end