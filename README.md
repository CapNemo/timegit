# README #

timegit ist dafür gedacht unsere Arbeitszeiten zentral in einem Git zu verwalten.  

***Die Aktuelle Version ist auch als Java-Version bei Downloads enthalten.  
Diese zeigt auch die Stunden pro Kalenderwoche.***

**Es gibt 4 Grundfunktionen:**

* Eine Aktivität hinzufügen
* Eine Aktivität löschen
* Beschreibung / Dauer/ Datum einer Aktivität modifizieren
* Synchronisation mit dem remote Repository

### Installation und Verwendung ###

* Konfiguration

  in der Datei ***settings.rb*** MUSS man den Pfad zum Zeitrepository setzten (oder stets davor hinein wechseln mit cd)

* Abhängigkeiten

  **Ruby** und **Git** müssen installiert sein um das Skript ausführen zu können

* Git konfigurieren

Das Programm verwendet den Git username und die Git email.
Daher müssen beide vorher festgelegt werden.

Dies geschieht mittels:

```
#!bash
git config --global user.name "Vorname Nachname"        #für den Benutzername
git config --global user.email "benutzer@beispiel.de"   #für die Emailadresse

```

Damit man nicht so oft sein Passwort eingeben muss wenn man HTTPS verwendet,  
empfiehlt es sich, das Passwort zu cachen.

```
#!bash
git config --global credential.helper cache

```


### Beispiele ###
* Änderungen vom Server holen

```
#!bash

ruby gittime.rb sync
```

* Eine Aktivität hinzufügen, die 30 Minuten gedauert hat

```
#!bash
ruby gittime.rb add "Dieses tolle Programm geschrieben" 30              #Protokolliert die Aktivität mit dem aktuellen Datum
ruby gittime.rb add "Dieses tolle Programm geschrieben" 30 17.04.2015   #Analog zu eben, jedoch mit Datum 17.04.2015
```

* Die Aktivität mit der ID 2 löschen
```
#!bash

ruby gittime.rb remove 2
```


* Die Änderungen auf den Server übertragen
```
#!bash

ruby gittime.rb sync
```

* Nach manuellem editieren der Aktivitaeten.txt das HTML-Protokoll neu generieren
```
#!bash

ruby gittime.rb render
```

### Fragen oder Probleme? ###
Am besten per XMPP an **capnemo@jabber.de** stellen