#!/usr/bin/ruby
unless Kernel.respond_to?(:require_relative)
  module Kernel
    # Dies wird gebraucht um require_relative in alten Ruby-Versionen (z.B. 1.8.6) nachzuruesten
    def require_relative(path)
      current_path = File.dirname(caller[0])
      abs_path     = File.expand_path("#{path}.rb", current_path)
      # noinspection RubyResolve
      require(abs_path)
    end
  end
end

#Wird genutzt um Umlaute unter Windows korrekt formatieren zu koennen
class String
  def cp_850_to_utf8
    if self.encoding.to_s == 'CP850'
      self.encode('UTF-8')
    else
      self
    end
  end
end

require_relative('lib/Git')
require_relative('lib/GitTime')
require_relative('conf/settings')

class ConfigError < StandardError
end

class Main
  ARG_ADD_DESCRIPTION     ='Beschreibung'
  ARG_ADD_DURATION        ='Dauer in Minuten'
  ARG_DATE                ='Datum (dd.mm.yyyy): ENTER fuer das aktuelle Datum'
  ARG_MOD_DESCRIPTION     ='Neue Beschreibung (ENTER fuer keine Aenderung)'
  ARG_MOD_DURATION        ='Dauer in Minuten (ENTER fuer keine Aenderung)'
  HELP                    ='Richtige Verwendung ruby gittime.rb [add/remove/modify/render/sync]'
  MSG_NO_WORKING_DIRECTORY='Warnung: $working_directory nicht gefunden --> conf/settings.rb bearbeiten!'
  MSG_SYNC_ERROR          = "Fehler mit der automatischen Synchronisation.\nEs wird eine Internetverbindung benoetig."

  # Constructor
  # @param [Array] argv --this should be the Sytems ARGV
  def initialize(argv)
    if $working_directory.nil? || !(File.exist?($working_directory))
      raise ConfigError.new(MSG_NO_WORKING_DIRECTORY)
    end
    Git.goto_project_dir
    Git.init
    self.autopull_wo_error
    @argv=argv
    @argc=argv.length
    @app =GitTime.new(Git.get_username)
  end

  # This function uses supplied Arguments from ARGV or asks for them
  # in the commandline
  # @return [Array]
  # @param [Array] arg_names
  # @param [Fixnum] offset
  def get_args(arg_names, offset=0)
    result=Array.new(arg_names.length)
    # noinspection RubyForLoopInspection
    for i in 0..arg_names.length-1
      if @argv[i+offset].nil?
        print "#{arg_names[i]}: "
        input    =$stdin.gets.chomp
        input    =input.cp_850_to_utf8
        result[i]=input
      else
        input    =@argv[i+offset]
        input    =input.cp_850_to_utf8
        result[i]=input
      end
    end
    result
  end

  def add_activity
    description, duration, date = get_args([ARG_ADD_DESCRIPTION, ARG_ADD_DURATION, ARG_DATE], 1)
    duration                    =duration.to_i
    if date.empty?
      @app.add_activity(description, duration)
    else
      date =Date.parse(date)
      @app.add_activity(description, duration, date)
    end
    autopush_wo_error
  end

  # So werden Konflikte vermieden,
  # aber Internetverbindung wird benoetigt
  def autopull_wo_error
    if $autosync
      puts 'Lade neue Aktivitaeten vom Server'
      begin
        Git.pull
      rescue Git::GitError
        puts MSG_SYNC_ERROR
      end
    end
  end

  def autopush_wo_error
    if $autosync
      puts 'Sende neue Aktivitaeten zum Server'
      begin
        Git.push
      rescue Git::GitError
        puts MSG_SYNC_ERROR
      end
    end
  end

  # @param [Fixnum] id
  def modify_activity(id)
    a = @app.get_activity(id)
    puts "Aktuell: #{a}"
    description=a.description
    duration   =a.duration
    date       =a.date
    input      = get_args([ARG_MOD_DESCRIPTION, ARG_MOD_DURATION, ARG_DATE], 2)
    description=input[0] unless input[0].empty?
    duration   =input[1].to_i unless input[1].empty?
    date       =Date.parse(input[2]) unless input[2].empty?
    @app.modify_activity(id, description, duration, date)
    autopush_wo_error
  end

  def start
    begin
      if @argc == 0
        puts "Zu wenige Argumente\n #{HELP}"
      else
        case @argv[0]
          when 'add'
            add_activity
          when 'remove'
            id=get_args(['ACTIVITY_ID'], 1)[0].to_i
            @app.remove_activity(id)
            autopush_wo_error
          when 'modify'
            id = get_args(['ID'], 1)[0].to_i
            modify_activity(id)
          when 'sync'
            Git.autosync
          when 'render'
            @app.generate_summary(true)
            autopush_wo_error
          else
            puts "Unbekannte Option!\n #{HELP}"
        end
      end
    rescue => e
      puts "#{e.class}: #{e}"
      puts e.backtrace if $debug_output
    end
  end
end

trap 'SIGINT' do
  puts 'Exiting'
  exit 130
end

begin
  main = Main.new(ARGV)
  main.start
rescue => e
  puts "Fehler: #{e.class}: #{e}"
  puts e.backtrace
end