require_relative('Activity')
require_relative('exceptions')

#Liste, die Aktivitaeten aufnimmt und Zeitaufwaende summiert
class ActivityList
  MSG_UNKNOWN_ACTIVITY='Diese ID ist nicht vergeben'
  #Konstruktor
  def initialize
    @activities = Array.new
    @next_id    = 1
  end

  def each(&block)
    @activities.each(&block)
  end

  #gibt die naechste freie id zurueck
  def get_next_id
    @next_id
  end

  #fuegt der Liste eine Aktivitaet hinzu
  def add_activity(activity)
    @activities.push(activity)
    @next_id = activity.id+1 if activity.id >= @next_id
  end

  #Loescht eine Aktivitaet aus der Liste und gibt sie zurueck oder nil falls nicht vorhanden
  def remove_activity(id)
    result = nil
    @activities.each do |a|
      if a.id === id
        result = a
        @activities.delete(a)
        break
      end
    end
    result
  end

  #gibt zu einer id die passende Aktivitaet oder nil zurueck falls nicht vorhanden
  def get_activity(id)
    return @activities[id] if @activities[id].id == id unless @activities[id].nil?
    @activities.each do |a|
      return a if a.id === id
    end
    raise UnknownActivityException.new(MSG_UNKNOWN_ACTIVITY)
  end

  def to_serializable_s
    result=''
    @activities.each do |a|
      result += a.to_serializable_s.chomp + "\n"
    end
    result
  end

  #Gibt die Summe der Arbeitszeiten zurueck
  def sum_minutes
    sum=0
    @activities.each do |a|
      sum+=a.duration
    end
    sum
  end

  def clear
    @activities.clear
    @sum_minutes=0
    @next_id=1
  end
end
