class HtmlElement
  def initialize(tag,content)
    @content=content.to_s
    @tag=tag
  end
  
  def add_content(content)
    @content += "\n" + content.to_s
  end
  
  def to_s
    "<#{@tag}>#{@content}<\/#{@tag}>"
  end
end