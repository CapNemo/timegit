require_relative('Html')

class Summary
  ACTIVITY_ID='ID'
  DESCRIPTION='Beschreibung'
  DURATION   ='Dauer'
  DATE       ='Datum'
  WEEK ='Kalenderwoche'
  STYLE      ='table,th,td {border: 2px solid black;} table {border-collapse: collapse;} td {padding: 5px; word-wrap: break-word;}'

  def min_kw
    min=100
    @activities.each do |a|
      min = a.cweek if a.cweek < min
    end
    min
  end

  def max_kw
    max=0
    @activities.each do |a|
      max = a.cweek if a.cweek > max
    end
    max
  end

  def initialize(user, activities)
    @user =user
    @activities=activities
    @min_kw=min_kw
    @max_kw=max_kw
  end

  def gen_html_weeksummary
    weeks=Array.new(@max_kw)
    @activities.each { |a|
      weeks[a.cweek]=0 if weeks[a.cweek].nil?
      weeks[a.cweek]+=a.duration
    }
    div=HtmlElement.new('div', "<h3>Stunden pro Kalenderwoche<\/h3>")
    table=HtmlElement.new('table', '')
    tr =HtmlElement.new('thead', sprintf("<th>%s<\/th><th>%s<\/th>", 'Kalenderwoche', 'IST-Stunden'))
    table.add_content(tr)
    for i in 0..weeks.length
      unless weeks[i].nil? || weeks[i]==0
          tr =HtmlElement.new('tr', sprintf("<td>%d<\/td><td>%d<\/td>", i, (weeks[i]/60.0).round))
          table.add_content(tr)
        end
    end
    div.add_content table
    div
  end

  def to_html
    doctype='<!DOCTYPE HTML>'
    html  =HtmlElement.new('html', '')
    head  =HtmlElement.new('head', '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">')
    style =HtmlElement.new('style', STYLE)
    head.add_content(style)
    html.add_content(head)
    body =HtmlElement.new('body', "<h1>Zeitaufwand #{@user}<\/h1>")
    table=HtmlElement.new('table', '')
    tr =HtmlElement.new('tr', sprintf("<th>%s<\/th><th>%s<\/th><th>%s<\/th><th>%s<\/th><th>%s<\/th>", ACTIVITY_ID, DESCRIPTION, DURATION, DATE, WEEK))
    table.add_content(tr)
    @activities.each do |a|
      tr=HtmlElement.new('tr', sprintf("<td>%d<\/td><td>%s<\/td><td>%d<\/td><td>%s<\/td><td>%s<\/td>", a.id, a.description, a.duration, a.date, a.cweek))
      table.add_content(tr)
    end
    body.add_content(table)
    strong=HtmlElement.new('strong', sprintf('Gesamtaufwand bisher: %.2f Stunden', @activities.sum_minutes.to_f/60.0))
    body.add_content(strong)
    body.add_content(gen_html_weeksummary)
    html.add_content(body)
    "#{doctype}\n#{html}"
  end

  def save_html(path)
    File.open(path, 'w') do |file|
      file.write(to_html.to_s)
    end
  end
end