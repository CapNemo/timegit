require_relative('../conf/settings')

module Git
  MSG_NO_GIT        = 'Git ist nicht installiert'
  MSG_NO_GIT_NAME   = 'Es wurde kein username gesetzt'
  MSG_NO_GIT_EMAIL  = 'Es wurde keine emailadresse gesetzt'
  MSG_ERROR_UNKNOWN = 'Ein unbekannter Fehler ist aufgetreten'
  class GitError < StandardError
    def initialize(error_code, msg)
      super(sprintf('ERR_%d: %s', error_code, msg))
    end
  end

  # Gibt den per git config user.name gesetzten Benutzernamen zurueck
  def self.get_username
    result = `git config user.name`
    raise Git::GitError.new($?.exitstatus, MSG_NO_GIT_NAME) if $?.exitstatus != 0
    result.chomp!
  end

  # Gibt die per git config user.email gesetzte Emailaddresse zurueck
  def self.get_email
    result = `git config user.email`
    raise Git::GitError.new($?.exitstatus, MSG_NO_GIT_EMAIL) if $?.exitstatus != 0
    result.strip
  end

  # Gibt für den aktuellen benutzer das Log in Kurzform aus
  def self.get_git_log(name)
    result = exec_git_command("git log --decorate=no --oneline --author \"#{name}\"", true)
    result.strip
  end

  # Wechselt in das in der settings.rb spezifizierte Verzeichnis
  def self.goto_project_dir
    if (not $working_directory.nil?) and File.exist?($working_directory)
      Dir.chdir($working_directory)
    end
  end

  # Commitet eine oder mehrere Dateien mit einer Nachricht und der Option zum direkten push
  def self.commit(files, message, push=false)
    files = [files] unless files.kind_of?(Array)
    files.each do |file|
      exec_git_command("git add \"#{file}\"")
    end
    exec_git_command("git commit -m \"#{message}\"")
    self.push if push
  end

  # Fuehrt ein git pull --rebase aus
  def self.pull_rebase(remote='origin', branch='master')
    exec_git_command("git pull --rebase #{remote} #{branch}")
  end

  # Fuert ein git pull (ohne rebase) aus
  def self.pull(remote='origin', branch='master')
    exec_git_command("git pull #{remote} #{branch}")
  end

  # git push
  def self.push(remote='origin', branch='master')
    exec_git_command("git push #{remote} #{branch}")
  end

  def self.init(working_dir=nil)
    unless is_a_git_repo?(working_dir)
      old_pwd=Dir.pwd
      Dir.chdir(working_dir) unless working_dir.nil?
      exec_git_command('git init')
      Dir.chdir(old_pwd) unless working_dir.nil?
    end
  end

  def self.is_a_git_repo?(working_dir=nil)
    working_dir=Dir.pwd if working_dir.nil?
    File.directory? File.join(working_dir, '.git')
  end

  # Fuehrt nacheinander einen pull mit rebase, dann einen push aus
  def self.autosync
    self.pull
    self.push
  end

  def self.exec_git_command(command, ret_msg=false)
    result_msg=%x<#{command}>
    result_msg=MSG_ERROR_UNKNOWN if result_msg.empty?
    raise Git::GitError.new($?.exitstatus, result_msg) unless $?.success?
    result_msg if ret_msg
  end

end