require 'date'
require_relative('constraint')
class Activity
  attr_reader :duration, :date, :id, :description
  MSG_NEG_DURATION     ='Die Dauer in Minuten muss positiv sein.'
  MSG_EMPTY_DESCRIPTION='Die Beschreibung darf nicht leer sein'
  MSG_NOT_NUMERIC      ='ist keine Zahl.'
#id ist eine pro Benutzer eindeutige Nummer
# @param [int]    duration ist die Dauer in minuten
# @param [string] description ist der Aktivitaetsname/Beschreibung
  def initialize(id, description, duration, date)
    # DEAKTIVIERT, da so keine Strings mit numerischem Inhalt erkannt werden
    # Constraint.check_argument((id.is_a? Numeric), "#{id} #{MSG_NOT_NUMERIC}")
    @id              =id.to_i
    self.description =description
    self.duration    =duration.to_i
    self.date        =date.to_s
  end

# @param [Date] d
  def date=(d)
    @date=Date.parse(d.to_s)
  end

# @param [String] d
  def description=(d)
    Constraint.check_argument(!d.empty?, MSG_EMPTY_DESCRIPTION)
    @description=d
  end

# @param [Fixnum] d duration in minutes (>= 0)
  def duration=(d)
    Constraint.check_argument(d >= 0, MSG_NEG_DURATION)
    @duration=d
  end

  def cweek
    @date.cweek
  end

# @return [string]
  def to_serializable_s
    sprintf('%s#%s#%d#%s', @id, @description, @duration, @date)
  end

  def to_s
    "ID: #{@id}, Description: #{@description}, Duration: #{@duration}, Date: #{@date}"
  end

end