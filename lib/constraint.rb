module Constraint
  def self.check_argument(bool, msg)
    raise ArgumentError.new(msg) unless bool
  end
end