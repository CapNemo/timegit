require_relative('Git')
require_relative('Activity')
require_relative('Summary')
require_relative('ActivityList')
require_relative('exceptions')
require 'fileutils'
require 'date'

#Klasse zum verwalten von Arbeitszeiten mit Plaintext und Git
class GitTime
  ACTIVITY_FILE_NAME         = 'Aktivitaeten.txt'
  PROTOCOL_FILE_NAME         = 'Protokoll.html'
  MSG_NO_ACTIVITY_FILE       = 'Es wurde noch keine Aktivitaetenliste erstellt, oder sie wurde geloescht'
  MSG_INVALID_ACTIVITY_STRING= 'Ungueltige Zeile in der Aktivitaetendatei'
  MSG_INVALID_ID             = 'Diese ACTIVITY_ID ist (noch) nicht vergeben!'
  MSG_UNALLOWED_CHAR         = 'Das #-Zeichen darf nicht in der Beschreibung enthalten sein!'
  MSG_UNKNOWN_ACTIVITY       = 'Zu dieser ACTIVITY_ID existiert keine Aktivitaet'
  MSG_AUTOMERGE              = 'Automerge Remote Activities'
  STATUS_OK                  = 0
  STATUS_MERGE_START         = 1
  STATUS_MERGE_MDDLE         = 2
  STATUS_MERGED              = 3
  #Konstruktor
  def initialize(user)
    @user              =user
    @activity_file_path= @user + '/' + ACTIVITY_FILE_NAME
    @proto_path        =@user + '/' + PROTOCOL_FILE_NAME
    @activity_list     = ActivityList.new
    load_activities
  end

  #Fuegt eine Aktivitaet hinzu
  #date wird zum aktuellen Datum falls nicht anders angegeben
  def add_activity(description, duration, date=date_now)
    raise InvalidActivityStringException.new(MSG_UNALLOWED_CHAR) if description.include? '#'
    date     =date_now if date.nil?
    activity = Activity.new(@activity_list.get_next_id, description, duration, date)
    @activity_list.add_activity(activity)
    self.save_activities
    files=[@activity_file_path, @proto_path]
    Git.commit(files, sprintf('ADD %d: %s', activity.id, activity.description))
  end

  #loescht eine bestimmte Aktivitaet
  def remove_activity(id)
    activity = @activity_list.remove_activity(id)
    raise UnknownActivityException.new(MSG_INVALID_ID) if activity == nil
    self.save_activities
    files=[@activity_file_path, @proto_path]
    Git.commit(files, sprintf('REMOVE%d', activity.id))
  end

  #aendert Parameter einer bestimmten Aktivitaet
  def modify_activity(id, new_description, new_duration, new_date=nil)
    a = @activity_list.get_activity(id)
    raise UnknownActivityException.new(MSG_UNKNOWN_ACTIVITY) if a.nil?
    a.description=new_description
    a.duration   =new_duration
    a.date       =new_date unless new_date.nil?
    self.save_activities
    files=[@activity_file_path, @proto_path]
    Git.commit(files, sprintf('MODIFY%d', a.id))
  end

  def get_activity(id)
    @activity_list.get_activity(id)
  end

  # Liest alle Aktivitaeten eines Nutzers aus seiner Aktivitaetsdatei und gibt ein Array von Aktivitaeten zurueck
  def load_activities
    save_activities unless File.exists?(@activity_file_path)
    status = STATUS_OK
    i=1
    File.open(@activity_file_path) do |file|
      file.each_line do |line|
        line.force_encoding(Encoding::UTF_8)
        i+=1
        if /\s*<<<<.*/ =~ line
          status = STATUS_MERGE_START
        elsif /\s*=======\s*/ =~ line
          status = STATUS_MERGE_MDDLE
        elsif /\s*>>>>.*/ =~ line
          status = STATUS_MERGED
        else
          split=line.split('#')
          raise InvalidActivityStringException.new("Zeile: #{i}: #{MSG_INVALID_ACTIVITY_STRING}") if split.length != 4
          id, description, duration, date=split
          if status == STATUS_MERGE_START or status == STATUS_MERGE_MDDLE
            id_old = id
            id     = @activity_list.get_next_id
            puts "Automerge: Rename Activity #{id_old}: #{description}\n\t==> #{id}"
          end
          @activity_list.add_activity(Activity.new(id, description, duration, date))
        end
      end
    end
    auto_merge if status === STATUS_MERGED
  end

  #Speichert die Aktivitaeten in die Aktivitaetsdatei
  def save_activities
    FileUtils.mkdir_p @user
    File.open(@activity_file_path, 'w+') do |file|
      file.write(@activity_list.to_serializable_s)
    end
    self.generate_summary
  end

  #generiert die Uebersicht als HTML Datei
  def generate_summary(commit=false)
    summary=Summary.new(@user, @activity_list)
    summary.save_html(@proto_path)
    Git.commit([@activity_file_path,@proto_path], "Edited activities manually") if commit
  end

  #Gibt das aktuelle Datum zurueck
  def date_now
    Date.parse(Time.now.to_s)
  end

  def auto_merge
    save_activities
    generate_summary
    Git.commit([@activity_file_path, @proto_path], MSG_AUTOMERGE)
    Git.autosync
  end

end