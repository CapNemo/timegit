#Fehlerklasse bei nicht existenten Dateien
class FileDoesNotExistError < IOError
end

#Fehlerklasse bei falsch formatierter Aktivitaetsdatei/Beschreibungen
class InvalidActivityStringException < StandardError
end

# Fehlerklasse bei nicht vorhandenen ids
class UnknownActivityException < StandardError
end